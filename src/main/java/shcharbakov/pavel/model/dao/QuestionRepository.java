package shcharbakov.pavel.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import shcharbakov.pavel.model.domain.Question;

public interface QuestionRepository extends JpaRepository<Question, String> {
}
