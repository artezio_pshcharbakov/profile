package shcharbakov.pavel.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import shcharbakov.pavel.model.domain.Department;

public interface DepartmentRepository extends JpaRepository<Department, String> {
}
