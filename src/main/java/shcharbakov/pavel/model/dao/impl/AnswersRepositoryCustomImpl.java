package shcharbakov.pavel.model.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.jdbc.core.JdbcTemplate;
import shcharbakov.pavel.model.dao.AnswersRepositoryCustom;
import shcharbakov.pavel.model.domain.Answer;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collections;
import java.util.List;

public class AnswersRepositoryCustomImpl implements AnswersRepositoryCustom {

    @PersistenceContext
    private EntityManager manager;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    @Query(value = "select ans from answer INNER JOIN question ON (questionid = question.guid) INNER JOIN profilequestions ON (question.guid = questionpk)\n" +
    "INNER JOIN profile ON (profilepk = profile.guid) WHERE profile.profilename=:profileTitle", nativeQuery = true)
    public List<String> findAnswers(@Param("profileTitle")String condition) {
        Answer asnwer = manager.find(Answer.class, condition);
        return Collections.singletonList(asnwer.getAnswer());
    }
}
