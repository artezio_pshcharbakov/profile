package shcharbakov.pavel.model.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AnswersRepositoryCustom {
    @Query(value = "select ans from answer INNER JOIN question ON (questionid = question.guid) INNER JOIN profilequestions ON (question.guid = questionpk)\n" +
            "INNER JOIN profile ON (profilepk = profile.guid) WHERE profile.profilename=:profileTitle", nativeQuery = true)
    List<String> findAnswers(@Param("profileTitle")String condition);
}
