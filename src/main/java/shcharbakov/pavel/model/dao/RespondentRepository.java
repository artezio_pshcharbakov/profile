package shcharbakov.pavel.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import shcharbakov.pavel.model.domain.Respondent;

public interface RespondentRepository extends JpaRepository<Respondent, String> {


}
