package shcharbakov.pavel.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import shcharbakov.pavel.model.domain.Profile;

public interface ProfileRepository extends JpaRepository<Profile, String> {

    Profile findByName(String name);
}
