package shcharbakov.pavel.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import shcharbakov.pavel.model.domain.Answer;

public interface AnswerRepository extends JpaRepository<Answer, String>, AnswersRepositoryCustom {
}
