package shcharbakov.pavel.model.service;

import shcharbakov.pavel.model.domain.Department;

import java.util.List;

public interface DepartmentService {

    List<Department> finAll();

    Department insertByBook(Department department);

    Department update(Department department);

    Department delete(String id);

    Department findById(String id);
}
