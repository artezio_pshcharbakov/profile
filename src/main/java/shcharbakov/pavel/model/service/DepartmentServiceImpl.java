package shcharbakov.pavel.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import shcharbakov.pavel.model.dao.DepartmentRepository;
import shcharbakov.pavel.model.domain.Department;

import java.util.List;

@Service
public class DepartmentServiceImpl implements DepartmentService{

    @Autowired
    DepartmentRepository departmentRepository;

    @Override
    public List<Department> finAll() {
        return departmentRepository.findAll();
    }

    @Override
    public Department insertByBook(Department department) {
        return departmentRepository.save(department);
    }

    @Override
    public Department update(Department department) {
        return departmentRepository.save(department);
    }

    @Override
    public Department delete(String id) {
        Department department = departmentRepository.findById(id).get();
        departmentRepository.delete(department);
        return department;
    }

    @Override
    public Department findById(String id) {
        return departmentRepository.findById(id).get();
    }
}
