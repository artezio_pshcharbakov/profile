package shcharbakov.pavel.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import shcharbakov.pavel.model.dao.AnswerRepository;

import java.util.List;

@Service
public class AnalyseAnswersService {

    @Autowired
    private AnswerRepository answersRepository;

    public double computeAnswers() {
        List<String> allAnswers = answersRepository.findAnswers("Результаты курса");
        double result = 0;
        int size = allAnswers.size();
        if (size != 0) {
            for (String str : allAnswers) {
                switch (str) {
                    case "y":
                        result += 3;
                        break;
                    case "s":
                        result++;
                        break;
                }
            }
        }
        return result / (size*1.);
    }
}
