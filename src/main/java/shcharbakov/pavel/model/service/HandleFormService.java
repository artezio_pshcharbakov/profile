package shcharbakov.pavel.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import shcharbakov.pavel.model.dao.AnswerRepository;
import shcharbakov.pavel.model.dao.QuestionRepository;
import shcharbakov.pavel.model.dao.RespondentRepository;
import shcharbakov.pavel.model.domain.Answer;
import shcharbakov.pavel.model.domain.FirstForm;
import shcharbakov.pavel.model.domain.Question;
import shcharbakov.pavel.model.domain.Respondent;

import java.util.Arrays;
import java.util.List;

@Service
public class HandleFormService {

    @Autowired
    private AnswerRepository answerRepository;
    @Autowired
    private RespondentRepository respondentRepository;
    @Autowired
    private QuestionRepository questionRepository;
    private FirstForm form;

    public HandleFormService() {
    }

    public void saveAnswers() {
        Respondent r = respondentRepository.findById("1").orElse(new Respondent("6", "Admin", "Admin"));
        Question[] qArr = {new Question(form.getEnjoyQuestion()), new Question(form.getAdviceQuestion()), new Question(form.getKnowledgeQuestion()),
                new Question(form.getQuickQuestion()), new Question(form.getTiredQuestion())};
        List<Question> questions = Arrays.asList(qArr);
        Answer[] ansArr = {new Answer(form.getEnjoy(), qArr[0], r), new Answer(form.getAdvice(), qArr[1], r), new Answer(form.getKnowledge(), qArr[2], r),
                new Answer(form.getQuick(), qArr[3], r), new Answer(form.getTired(), qArr[4], r)};
        List<Answer> answerList = Arrays.asList(ansArr);
        questionRepository.saveAll(questions);
        answerRepository.saveAll(answerList);
    }

    public void setForm(FirstForm form) {
        this.form = form;
    }
}
