package shcharbakov.pavel.model.domain;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "respondent")
public class Respondent extends BaseEntity{

    @Column(name = "sname", length = 128, nullable = false)
    private String name;
    @Column(name = "surname", length = 128, nullable = false)
    private String surName;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "facultyid")
    private Department faculty;

    public Respondent() {
    }

    public Respondent(String name, String surName) {
        this.name = name;
        this.surName = surName;
        setGuid(UUID.nameUUIDFromBytes((name+surName).getBytes()).toString());
    }

    public Respondent(String guid, String name, String surName) {
        setGuid(guid);
        this.name = name;
        this.surName = surName;
    }

    @Override
    public String toString() {
        return "Respondent{" + "name= " + name + ", surName= " + surName + ", facultyID='" + faculty + '}';
    }
}
