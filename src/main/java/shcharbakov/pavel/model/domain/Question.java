package shcharbakov.pavel.model.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "question")
public class Question extends BaseEntity {

    @Column(name = "question", length = 256, nullable = false)
    private String question;

    public Question() {
    }

    public Question(String question) {
        this.question = question;
        setGuid(UUID.nameUUIDFromBytes(question.getBytes()).toString());
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    @Override
    public String toString() {
        return "Question{ question= " + question + '}';
    }
}
