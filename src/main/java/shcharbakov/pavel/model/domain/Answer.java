package shcharbakov.pavel.model.domain;

import javax.persistence.*;

@Entity
@Table(name = "answer")
public class Answer extends BaseEntity {

    @Column(name = "ans", length = 255)
    private String answer;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "respondentid")
    private Respondent respondent;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "questionid")
    private Question question;

    public Answer() {
    }

    public Answer(String answer, Question question, Respondent respondent) {
        this.answer = answer;
        this.question = question;
        this.respondent = respondent;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Respondent getRespondent() {
        return respondent;
    }

    public void setRespondent(Respondent respondent) {
        this.respondent = respondent;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }
}
