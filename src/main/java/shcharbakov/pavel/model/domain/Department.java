package shcharbakov.pavel.model.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "departments")
public class Department extends BaseEntity{

    @Column(name = "title", length = 128)
    private String title;
    @OneToMany(mappedBy = "faculty", cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<Respondent> students = new ArrayList<>();

    public Department() {
    }

    public Department(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Department: title= " + title + ';';
    }
}
