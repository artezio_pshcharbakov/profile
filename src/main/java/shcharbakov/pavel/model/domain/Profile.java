package shcharbakov.pavel.model.domain;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "profile")
public class Profile extends BaseEntity{
    @Column(name = "profilename", length = 128, nullable = false)
    private String name;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Answer> answers;

    public Profile() {
    }

    public Profile(String name) {
        this.name = name;
        setName(UUID.nameUUIDFromBytes(name.getBytes()).toString());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }
}
