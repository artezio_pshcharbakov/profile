package shcharbakov.pavel.model.domain;

import org.springframework.cache.annotation.Cacheable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.UUID;

@MappedSuperclass
@Cacheable
public class BaseEntity implements Serializable {

    @Id
    @Column(name = "guid", nullable = false, unique = true)
    private String guid;

    public BaseEntity() {
        if(guid == null) {
            guid = UUID.randomUUID().toString();
        }
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    @Override
    public String toString() {
        return guid;
    }
}
