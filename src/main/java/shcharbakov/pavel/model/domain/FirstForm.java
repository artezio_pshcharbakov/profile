package shcharbakov.pavel.model.domain;

public class FirstForm {

    private final String enjoyQuestion = "Удовлетворены ли Вы объемом информации, которая поступала от преподавателя в период обучения на курсе?";
    private final String adviceQuestion = "К преподавателю курса можно было обратиться за советом и помощью в трудной ситуации?";
    private final String knowledgeQuestion = "На занятии преподаватель оценивал мои знания, а не мое поведение?";
    private final String quickQuestion = "Занятия для меня проходили очень быстро?";
    private final String tiredQuestion = "Я испытывал усталость на занятии?";

    private String enjoy;
    private String advice;
    private String knowledge;
    private String quick;
    private String tired;

    public String getEnjoy() {
        return enjoy;
    }

    public void setEnjoy(String enjoy) {
        this.enjoy = enjoy;
    }

    public String getAdvice() {
        return advice;
    }

    public void setAdvice(String advice) {
        this.advice = advice;
    }

    public String getKnowledge() {
        return knowledge;
    }

    public void setKnowledge(String knowledge) {
        this.knowledge = knowledge;
    }

    public String getQuick() {
        return quick;
    }

    public void setQuick(String quick) {
        this.quick = quick;
    }

    public String getTired() {
        return tired;
    }

    public void setTired(String tired) {
        this.tired = tired;
    }

    public String getEnjoyQuestion() {
        return enjoyQuestion;
    }

    public String getAdviceQuestion() {
        return adviceQuestion;
    }

    public String getKnowledgeQuestion() {
        return knowledgeQuestion;
    }

    public String getQuickQuestion() {
        return quickQuestion;
    }

    public String getTiredQuestion() {
        return tiredQuestion;
    }
}
