package shcharbakov.pavel.model.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import shcharbakov.pavel.model.domain.FirstForm;
import shcharbakov.pavel.model.service.AnalyseAnswersService;
import shcharbakov.pavel.model.service.HandleFormService;

import javax.validation.Valid;

@Controller
public class HandlerForm {

    private final String PAGE = "page";
    private final String MAIN_CONTENT = "mainContent";

    @Autowired
    private HandleFormService handleFormService;

    @Autowired
    private AnalyseAnswersService analyseAnswersService;

    @RequestMapping(value = "/handleFirstForm", method = RequestMethod.POST)
    public String submit(@Valid @ModelAttribute("questionaryFirst") FirstForm form,
                         BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            return "error";
        }

        handleFormService.setForm(form);
        handleFormService.saveAnswers();

        model.addAttribute("enjoy", form.getEnjoy());
        model.addAttribute("advice", form.getAdvice());
        model.addAttribute("knowledge", form.getKnowledge());
        model.addAttribute("quick", form.getQuick());
        model.addAttribute("tired", form.getTired());
        model.addAttribute(MAIN_CONTENT, "centreContent/thanks.jsp");
        return PAGE;
    }

    @RequestMapping(value = "/statisticForForm", method = RequestMethod.GET)
    public String computeAnswers(ModelMap model) {
        double result = analyseAnswersService.computeAnswers();
        model.addAttribute("mark", result);
        model.addAttribute(MAIN_CONTENT, "centreContent/result.jsp");
        return PAGE;
    }
}


