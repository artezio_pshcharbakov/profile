package shcharbakov.pavel.model.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import shcharbakov.pavel.model.dao.DepartmentRepository;
import shcharbakov.pavel.model.domain.Department;
import shcharbakov.pavel.model.domain.FirstForm;

import java.util.List;
import java.util.Map;

@Controller
public class WelcomeController {

    @Autowired
    private DepartmentRepository departmentRepository;

    private final String PAGE = "page";
    private final String MAIN_CONTENT = "mainContent";

    @RequestMapping("/")
    public String welcome(Map<String, Object> model) {
        List<Department> departments = departmentRepository.findAll();
        model.put("facultets", departments);
        model.put(MAIN_CONTENT, "centreContent/mainContent.jsp");
        return PAGE;
    }

    @RequestMapping(value = "/firstForm", method = RequestMethod.GET)
    public String toFirstForm(Map<String, Object> model) {
        FirstForm form = new FirstForm();
        model.put("questionaryFirst", form);
        model.put("EnjoyQuestion", form.getEnjoyQuestion());
        model.put("adviceQuestion", form.getAdviceQuestion());
        model.put("knowledgeQuestion", form.getKnowledgeQuestion());
        model.put("quickQuestion", form.getQuickQuestion());
        model.put("tiredQuestion", form.getTiredQuestion());
        model.put(MAIN_CONTENT, "centreContent/firstForm.jsp");
        return PAGE;
    }

    @RequestMapping("/leftMenu")
    public String leftMenu(Map<String, Object> model) {
        List<Department> departments = departmentRepository.findAll();
        model.put("facultets", departments);
        return "parts/left";
    }
}
