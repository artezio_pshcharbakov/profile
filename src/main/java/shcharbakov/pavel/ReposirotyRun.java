package shcharbakov.pavel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import shcharbakov.pavel.model.dao.AnswerRepository;

import java.util.List;

@Controller
@SpringBootApplication(scanBasePackages = {"shcharbakov.pavel.model.dao", "shcharbakov.pavel.model.service"})
public class ReposirotyRun {

    @Autowired
    AnswerRepository answerRepository;

    public static void main(String[] args) {
        SpringApplication.run(ReposirotyRun.class, args);
    }

    @RequestMapping("/dept")
    public void run() throws Exception {
        List<String> s = answerRepository.findAnswers("Результаты курса");
        s.forEach(System.out::println);
    }
}
