package shcharbakov.pavel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"shcharbakov.pavel.model.controller", "shcharbakov.pavel.model.service"})
public class OnlineProfileApplication {
    public static void main(String[] args) {
        SpringApplication.run(OnlineProfileApplication.class, args);
    }
}

