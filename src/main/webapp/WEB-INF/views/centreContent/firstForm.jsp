<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <title>Учреждение образования</title>
    <style>
        <%@include file="../styles/styles.css" %>
    </style>
</head>

<body>
<div id="content">
    <form:form method="POST" action="/handleFirstForm" modelAttribute="questionaryFirst">
        <table width="750px">
            <tr>
                <td><form:label path="enjoy">${EnjoyQuestion}</form:label></td>
                <td width="10%"><form:radiobutton path="enjoy" value="y" label="Да"/></td>
                <td width="10%"><form:radiobutton path="enjoy" value="n" label="Нет"/></td>
                <td width="18%"><form:radiobutton path="enjoy" value="s" label="Не всегда"/></td>
            </tr>
            <tr>
                <td><form:label path="advice">${adviceQuestion}</form:label></td>
                <td><form:radiobutton path="advice" value="y" label="Да"/></td>
                <td><form:radiobutton path="advice" value="n" label="Нет"/></td>
                <td><form:radiobutton path="advice" value="s" label="Не всегда"/></td>
            </tr>
            <tr>
                <td><form:label path="knowledge">${knowledgeQuestion}</form:label></td>
                <td><form:radiobutton path="knowledge" value="y" label="Да"/></td>
                <td><form:radiobutton path="knowledge" value="n" label="Нет"/></td>
                <td><form:radiobutton path="knowledge" value="s" label="Не всегда"/></td>
            </tr>
            <tr>
                <td><form:label path="quick">${quickQuestion}</form:label></td>
                <td><form:radiobutton path="quick" value="y" label="Да"/></td>
                <td><form:radiobutton path="quick" value="n" label="Нет"/></td>
                <td><form:radiobutton path="quick" value="s" label="Не всегда"/></td>
            </tr>
            <tr>
                <td><form:label path="tired">${tiredQuestion}</form:label></td>
                <td><form:radiobutton path="tired" value="y" label="Да"/></td>
                <td><form:radiobutton path="tired" value="n" label="Нет"/></td>
                <td><form:radiobutton path="tired" value="s" label="Не всегда"/></td>
            </tr>
            <tr>
                <td><input type="submit" value="Отправить"/><input type="reset" value="Сбросить"/></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </form:form>
    <form:form method="GET" action="/statisticForForm">
        <input type="submit" value="Результаты"/>
    </form:form>
</div>
</body>
</html>
