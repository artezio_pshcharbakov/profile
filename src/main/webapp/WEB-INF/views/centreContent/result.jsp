<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <meta charset="utf-8">
    <title>Title</title>
</head>
<body>
<div id="container">
    <div id="content">
        <h2>Средняя оценка по результатам анкеты:</h2>
        <table>
            <tr>
                <td>Средний результат:</td>
                <td>${mark}</td>
            </tr>
        </table>
    </div>
</div>
</body>
</html>