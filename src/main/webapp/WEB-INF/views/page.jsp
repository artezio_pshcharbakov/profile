<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <title>Учреждение образования</title>
    <style>
        <%@include file="styles/styles.css" %>
    </style>
</head>

<body>
<div class="pageWrapper">
    <header>
        <%@include file="parts/header.jsp" %>
    </header>
    <div class="contentWrapper">
        <div class="columnWrapper">
            <!-- основной контент -->
            <article class="main">
                <jsp:include page="${mainContent}"/>
            </article>
            <!-- левая боковая панель -->
            <aside class="sidebar1">
                <jsp:include page="/leftMenu"/>
            </aside>
        </div>
        <!-- правая боковая панель -->
        <aside class="sidebar2">
            <%@include file="parts/right.jsp" %>
        </aside>
    </div>
    <footer>
        <%@include file="parts/footer.jsp" %>
    </footer>
</div>
</body>
</html>